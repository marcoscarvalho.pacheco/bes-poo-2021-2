package pooLista;

import java.util.Scanner;

public class Questao02_Aula02_09 {

	public static void main(String[] args) {

		/*
		 * Crie um programa em Java que l� um valor N, inteiro e positivo, calcula e
		 * escreve o valor de E (soma dos inversos dos fatoriais de 0 a N): 
		 * E = 1 + 1 / 1! + 1 / 2! + 1 / 3! + ... + 1 / N! == 
		 * E = 1/0! + 1/1! + 1/2! + 1/3! ... 1/N! 
		 * Ps: (0! = 1)
		 */

		// IN
		// numero N inteiro positivo

		// PROCESSAAMENTO
		// Soma dos inversos fatoriais = E
		// Usar exemplos, quando o problema est� muito fora de nosso dom�nio

		// Ex:
		/*
		 * N = 3 E = ? 
		 * E = 1/0! + 1/1! + 1/2! + 1/3! 
		 * E = 1/1 + 1/1 + 1/2 + 1/6 
		 * E = 1 + 1 + 0.5 + 0.167 
		 * E = 2.667
		 */

		// OUT
		// Exibir a soma(E)

		int n = inteiroPositivo();
		double e = calcularValorE(n);
		ExibirE(n, e);

	}

	private static void ExibirE(int a, double b) {
		// exibirE(n, e);
		System.out.println("E("+a+") = " + b);

	}

	private static int inteiroPositivo() {
		Scanner read = new Scanner(System.in);
		int n;
		do {
			System.out.println("Informe um n�mero inteiro e positivo:");
			n = read.nextInt();
			if (n < 0) {
				System.out.println("Numero fora da faixa");
			}
		} while (n < 0);
		return n;
	}

	

	private static double calcularValorE(int n) {
		double e = 0;
		for (int i = 0; i <= n; i++) {
			long fat = calcularFatorial(i);
			e = e + 1d / fat; // e = e + 1/i fatorial ..::.. estou usando a letra "d" para o compilador entender que estou usando o tipo double
		}
		return e;
	}



	private static long calcularFatorial(int i) {
		long fat = 1;
		for (int j = 1; j <= i; j ++) {
			fat = fat * j;
		}
		return fat;
	}
}
		
		

