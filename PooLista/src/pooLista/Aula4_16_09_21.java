package pooLista;

public class Aula4_16_09_21 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// ALT + SHIT + R >> renomear variaveis, classes, qualquer coisa.

		Aluno aluno1 = new Aluno();
		Aluno aluno2 = new Aluno();
		int qtd = 10;
		Aluno[] alunoS = new Aluno[qtd];
		
		aluno1.nome = "Marcos";
		aluno2.nome = "Maria";
		alunoS[3] = new Aluno();
		alunoS[3].nome = "Marcellus"; 
		alunoS[3].email = "mcp_marcos@hotmail.com";
		alunoS[3].telefone = "6655441122";
		
		//System.out.println("aluno.nome=" + aluno1.nome);
		//System.out.println(alunoS[0]);
		
		/*Nesse exemplo criei uma instancia da classe "Aluno" na quarta posi��o do vetor "alunoS". 
		 * Em seguida, printei todas as posi��es do vetor, apenas para ver o "null" armazenado em todas as posi��es do vetor, 
		 * com excess�o do da quarta posi��o(�ndice 3)
		 */
		
		for (int i = 0; i < qtd; i++) {
			System.out.println("alunos[" + i + "]" + alunoS[i]);
		}
		System.out.println("alunoS[3].matricula = " + alunoS[3].matricula);
		System.out.println("alunoS[3].nome = " + alunoS[3].nome);
		System.out.println("alunoS[3].email = " + alunoS[3].email);
		System.out.println("alunoS[3].telefone = " + alunoS[3].telefone);
	
		
		aluno1.matricula = 123123;
		aluno1.nome = "marcos";
		aluno1.telefone = "123456789";
		aluno1.email = "mcp_marcos...";
		
		aluno2.matricula = 9876789;
		aluno2.nome = "lui";
		aluno2.telefone = "60504050600";
		aluno2.email = "max.plini@gmail.com";
		
		if(aluno1 == aluno2) {
			System.out.println("aluno1 � igual ao aluno2");
		}else {
			System.out.println("aluno1 � difetrente do aluno2");
		}
		
		// A qui, o resultado da condicional sempre ser� "diferente", pois dentro de "aluno1" � armazenado o endere�o da inst�ncia 
		// na mem�ria heap. Da mesma forma com "aluno2", por ser uma outra inst�ncia, ter� um outro endere�o.
		
		
		System.out.println("\naluno1: " + aluno1);
		System.out.println("aluno2: " + aluno2);
	
	}
}
