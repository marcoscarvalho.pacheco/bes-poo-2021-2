package dominio;



public class Veiculo {

	// ALT + SHIFT + R
	// Seleciona uma palavra que o cursor esteja em cima e ao renomea-la, automaticamente o novo nome tamb�m substituir� 
	// todas as men��es � palavra anterior.
	
	//FIXME  Os atributos form definidos como publicos PROVISORIAMENTE, enquanto n�o estudei ENCAPSULAMENTO.
	
	// Quando n�o definimos o modificador de visibilidade, a "visibilidade default" � uma visibilidade de pacote, ou seja, 
	// apenas classes do mesmo pacote podem manipular um atributo,
	// quando esse atributo n�o tem modificador de visibilidade(tbm chamad de modificador de acesso) definido para ele.
	//Ex: Double calculoIm posto; >> Escrevi o tipo da variavel diretamente, sem escrever antes o modificador de acesso.
	
	
	private String placa;// A placa ser� vis�vel apenas pela classe ve�culos e por outras classses dentro do mesmo pacote.
	private Integer anoFabric; // poderia e deveria usar o Integer
	private Integer anoModelo;
	private double valor;
	private Pessoa proprietario;
	private Pessoa condutor;
	//public Double icms; (exemplo dado na aula do dia 23/09 2h28:15)
	
	public Veiculo() {
		
	}
	
	
	//ALT + SHIFT + S Gerador de  Construtores com camppos para par�metros
	
	/*Ao criar construtores parametrizados, precisamos criar novamente um que n�o tenha par�metors, 
	 * pois o construtor implicito e invisivel(que n�o precisa de par�metros) desaparece.*/
	
	public Veiculo(String placa) {
		this.placa = placa.toUpperCase();
	}
	
	public Veiculo(String placa, Double valor) {
		/*No caso abaixo uso a palavra reservada "this" para sinalizar que o identificador "placa" aponta para o 
		 * atributo(nesse caso, um hom�nimo) "placa", da INST�NCIA ATUAL, assim como outros dois atributos. 
		 */
		this(placa);
		setValor(valor);
		//this(placa); 
		/*Nesse caso, usando a palavra reservada this, abrindo parenteses e colcando o parametro "placa", durante essa instancia��o, 
          ser�o executadas as regras que est�o no construtor 
		  que tem apenas "placa" (Umpar�metro de tipo String) no par�metro. Isso tambem funciona com mais de um par�metro*/
				
		
	}
	
	public Veiculo(String placa, Integer anoFabricacao, Double valor) {
	
		this(placa, valor);
		this.anoFabric = anoFabricacao;
		
		//System.out.println("parametroPlaca="+parametroPlaca);
		//System.out.println("parametroFabricacao="+parametroFabricacao);
		//System.out.println("parametroValor="+parametroValor);
		
	}
	
	
	public void setValor(Double valor) {
		if(valor > 0) {
			this.valor = valor;
			} else {
			this.valor = 0d;
			}
	}
	
	/*N�o posso fazer 2 construtores que tenham EXATAMENTE a mesma quantidade de par�metros, os mesmos tipos e na mesma ordem. Como no exemplo ABAIXO; */
	/*Em processos de SOBRECARGA, tanto de m�todos quanto de construtores, o compilador observa a TIPAGEM dos par�metros.  */
	
	
	
	/*public Veiculo(Integer anoFabricacao) {
		this.anoFabric = anoFabricacao;
	}

	public Veiculo(Integer anoModelo) {
		this.anoFabric = anoModelo;
	}*/
	
	public Double calcularImposto() {
		return this.valor*0.1;
		
	}
	
	/*Abaixo, exemplos de m�todos que servem para "recuperar"(consultar) esse valor, sem alterar o valor, apenas consultar
	 
	
	//Getters 

	public String obterPlaca(){
		
		return placa;
	}
	
public Integer obterAnoFabricacao(){
		
		return anoFabric;
	}
	
public Double obterValor(){
	
	return valor;
} */

//Setters
//Exemplo de um m�todo que altere o valor de determinado atributo




/*Abaixo, testei o atalho ALT SIFT S para criar apenas os getters de todos os atrbutos, poderia tamb�m no mesmo ato criar os setters, ou n�o..*/

public String getPlaca() {
	return placa;
}



public void setPlaca(String placa) {
	this.placa = placa;
}


public void setAnoFabric(Integer anoFabric) {
	this.anoFabric = anoFabric;
}


public void setProprietario(Pessoa proprietario) {
	this.proprietario = proprietario;
}


public Integer getAnoFabric() {
	return anoFabric;
}


public Integer getAnoModelo() {
	return anoModelo;
}


public double getValor() {
	return valor;
}


public Pessoa getProprietario() {
	return proprietario;
}


public Pessoa getCondutor() {
	return condutor;
}


@Override
public String toString() {
	return "Veiculo [placa=" + placa + ", anoFabric=" + anoFabric + ", anoModelo=" + anoModelo + ", valor=" + valor
			+ ", proprietario=" + proprietario + ", condutor=" + condutor + "]";
}
	

	
	
	
}
