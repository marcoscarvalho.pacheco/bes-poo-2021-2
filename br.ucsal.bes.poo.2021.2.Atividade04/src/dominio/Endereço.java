package dominio;

public class Endere�o {

	private String logradouro;
	private String cep;
	private String complemento;
	private String bairro;  
	
	//ALT + SHIFT + S
	//Usar esse atalho para gerar m�todos Getters e Setters.
	
	
	//Exemplo de um Getter. M�todo que serve para retornar um atributo
	public String getCep() {
		return cep;
	}
	
	//M�todo que permite a altera��o do cep:
	
	public void setCep(String cep) {
		
		//Contruir depois, uma l�gica aqui dentro.
		this.cep = cep;
	}
	
	//M�todo que permite a altera��o do bairro:
	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Endere�o(String logradouro, String cep, String complemento, String bairro) {
		//super();
		this.logradouro = logradouro;
		this.cep = cep;
		this.complemento = complemento;
		this.bairro = bairro;
	}
}
