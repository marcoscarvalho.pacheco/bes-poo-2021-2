package dominio;

public class Pessoa {

	private static final int QTD_MAX_TELEFONES = 10;
	private String nome;
	private String cpf;
	private Enderešo enderešo;
	private String[] telefones = new String[QTD_MAX_TELEFONES];;
	private int qtdTelefones = 0;
	
	
	public void addTelefones(String telefone) {
		telefones[qtdTelefones] = telefone;
		qtdTelefones ++;
	}


	public String getNome() {
		return nome;
	}


	public String getCpf() {
		return cpf;
	}


	public Enderešo getEnderešo() {
		return enderešo;
	}


	public String[] getTelefones() {
		return telefones;
	}


	public int getQtdTelefones() {
		return qtdTelefones;
	}
	

}
