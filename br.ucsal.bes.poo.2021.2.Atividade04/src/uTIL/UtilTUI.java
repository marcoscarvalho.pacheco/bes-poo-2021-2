package uTIL;

import java.util.Scanner;

public class UtilTUI {

	private static final Scanner read = new Scanner(System.in); 
	
	public static double obterDouble(String msg) {
		System.out.println(msg);
		double  numero= read.nextDouble();
		read.nextLine();
		return numero;
	}

	public static Integer obterInteger(String msg) {
		System.out.println(msg);
		int numero = read.nextInt();
		read.nextLine();
		return numero;
		
	}
	public static String obterString(String msg) {
		System.out.println(msg);
		return read.nextLine();
		
	}
	
}
