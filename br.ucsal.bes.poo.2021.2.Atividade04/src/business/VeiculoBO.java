package business;

import java.time.LocalDate;

import dominio.Veiculo;
import persistence.VeiculoArrayDAO;

public class VeiculoBO {
	
	public static String incluir(Veiculo veiculo) {
	    //FIXME Usar ano atual no lugar de uma constante. local Date.
		
		int anoAtual = LocalDate.now().getYear();
		int aFabricacao = veiculo.getAnoFabric();
		
		if(aFabricacao > anoAtual) {
			// Se o ano de fabrica��o for maior que o ano atual, ent�o a regra desse neg�cio diz
			// que esse veiculo N�O pode ser incluido.
			
			// System.out.println("ERRO");
			
			// FIXME Substituir essa implementa��o pelo uso de Exceptions
			
			return "ERRO: O ano e fabrica��o n�o pode ser maior que o ano atual";
		}
		
		// DAO > Data Acess Object
		//veiculo.icms = 0.25 * veiculo.valor; (exemplo dado na aula do dia 23/09 2h28:15)
		VeiculoArrayDAO.insert(veiculo);
		
		return "Sucesso"; 
	}
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
	}

}
