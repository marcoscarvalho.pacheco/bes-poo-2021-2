package t_u_i;



import business.VeiculoBO;
import dominio.Veiculo;
import persistence.VeiculoArrayDAO;
import uTIL.UtilTUI;

public class VeiculoTUI {

	
		
		
	public static void incluir() {
		System.out.println("##############>>> INCLUS�O DE VE�CULOS <<<###############");
		
		String placa = UtilTUI.obterString("Informe a placa: ");
		int ano = UtilTUI.obterInteger("Informe o ano de fabrica��o: ");
		double valor = UtilTUI.obterDouble("Informe o valor: ");
		
		//TODO Refatorar para utilizaar construtor parametrizado.		
		
		Veiculo veiculo = new Veiculo(placa, ano, valor);
		
		
		veiculo.calcularImposto();
		
		String retorno = VeiculoBO.incluir(veiculo);
		System.out.println(retorno);
	}
		
	public static void listar() {
		System.out.println("##############>>> LISTA DE VE�CULOS <<<###############");
		Veiculo[] veiculos = VeiculoArrayDAO.findAll();
		for (int i = 0; i < veiculos.length; i++) {
			System.out.println(veiculos[i]);
			System.out.println("\nPlaca = " + veiculos[i].getPlaca());
			System.out.println("Ano de fabrica��o = " + veiculos[i].getAnoFabric());
			System.out.println("Valor = " + veiculos[i].getValor());
		}
	}
	
	

}
	
