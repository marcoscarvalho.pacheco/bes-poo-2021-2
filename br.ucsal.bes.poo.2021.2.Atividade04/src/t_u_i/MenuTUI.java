package t_u_i;


import uTIL.UtilTUI;

//FIXME Criar um construtor privado quando explicar o que s�o m�todos de classe e m�todos de inst�ncia.

public class MenuTUI {

	private static final int INCLUIR_PROPRIETARIO = 1;
	private static final int INCLUIR_VEICULO = 2;
	private static final int LISTAR_VEICULOS = 3;
	private static final int SAIR = 9;
	
	public static void executar() {
 
		int opcaoSelecionada;
		do {

			exibirOpcoes();
			opcaoSelecionada = obterOpcaoSelecionada();
			executarOpcao(opcaoSelecionada);
		} while (opcaoSelecionada != SAIR);
	}

	public static int obterOpcaoSelecionada() {
		int opcaoSelec = UtilTUI.obterInteger("Informe a op��o selecionada: ");
		return opcaoSelec;
	}
		

	public static void exibirOpcoes() {

		System.out.println(INCLUIR_PROPRIETARIO + " - incluir proprietario");
		System.out.println(INCLUIR_VEICULO + " - incluir ve�culo");
		System.out.println(LISTAR_VEICULOS + " - Listar ve�culos ");
		System.out.println(SAIR + " - Sair");
	}

	public static void executarOpcao(int opcS) {
		switch (opcS) {
		case INCLUIR_PROPRIETARIO:
			System.out.println("Aqui vou chamar um metodo para adicionar propriet�rio.");
			
			break;
		case INCLUIR_VEICULO:
			//System.out.println("Aqui vou chamar um metodo para adicionar em ve�culo.");
			VeiculoTUI.incluir();
			break;
		case LISTAR_VEICULOS:
			//System.out.println("Aqui vou chamar um metodo para que o usu�rio visualise a lista de ve�culos.");
			VeiculoTUI.listar();
			break;
		case SAIR:
			System.out.println("Adeus..");
			break;
		default:
			System.out.println("Op��o inv�lida.");
		 	break;
		}
	}

}
