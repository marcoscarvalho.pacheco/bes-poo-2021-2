package t_u_i;

import java.util.List;

import business.VeiculoBO;
import dominio.Veiculo;
import persistence.VeiculoListDAO;
import uTIL.UtilTUI;

public class VeiculoListTUI {

	
	
	public static void incluir() {
		System.out.println("##############>>> INCLUS�O DE VE�CULOS <<<###############");
		
		String placa = UtilTUI.obterString("Informe a placa: ");
		int ano = UtilTUI.obterInteger("Informe o ano de fabrica��o: ");
		double valor = UtilTUI.obterDouble("Informe o valor: ");
		
		//TODO Refatorar para utilizaar construtor parametrizado.		
		
		Veiculo veiculo = new Veiculo(placa, ano, valor);
		
		
		veiculo.calcularImposto();
		
		String retorno = VeiculoBO.incluir(veiculo);
		System.out.println(retorno);
	}
		
	public static void listar() {
		System.out.println("##############>>> LISTA DE VE�CULOS <<<###############");
		List<Veiculo> veiculos = VeiculoListDAO.findAll();
		for (Veiculo veiculo: veiculos) {
			
			System.out.println("\nPlaca = " + veiculo.getPlaca());
			System.out.println("Ano de fabrica��o = " + veiculo.getAnoFabric());
			System.out.println("Valor = " + veiculo.getValor());
		}
	}
}
