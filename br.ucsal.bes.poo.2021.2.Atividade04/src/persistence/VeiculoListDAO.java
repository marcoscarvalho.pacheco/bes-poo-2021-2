package persistence;

import java.util.ArrayList;
import java.util.List;

import dominio.Veiculo;

public class VeiculoListDAO {

	
			// Refatorando a classe VeiculoArrayDAO, usando LIST e ArrayList

	private static List<Veiculo> veiculos = new ArrayList<>();
	

	public static void insert(Veiculo veiculo) {
		veiculos.add(veiculo);
	}

	public static void remove(int posicao) {
		veiculos.remove(posicao);
	}

	public static Veiculo findByIndex(int pos) {
		return veiculos.get(pos);
	}
	
	public static Veiculo findByPlaca(String placa) {
		for (Veiculo veiculo: veiculos) {
			if(veiculo.getPlaca().equals(placa)) {
				return veiculo;
			}
		}
		return null;
			
	}
	/* ESTUDAR forEACH
  -->> public static Veiculo findByPlaca(String placa) {
		for (Veiculo veiculo : veiculos) {
			System.out.println("Veiculo.placa = " + veiculo.getPlaca());
		}
		return null;
	}*/
	
	public static List<Veiculo> findAll() {
		return new ArrayList<>(veiculos);
	}
		
		
}
