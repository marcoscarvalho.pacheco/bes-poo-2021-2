package persistence;

import dominio.Veiculo;

public class VeiculoArrayDAO {

	private static final int QTD_MAX_VEICULOS = 1000;

	private static Veiculo[] veiculos = new Veiculo[QTD_MAX_VEICULOS];
	private static int qtd = 0;

	public static void insert(Veiculo veiculo) {
		if(qtd > QTD_MAX_VEICULOS) {
			// crescer meu array, criar um maior e copiar os dados do array atual para o array maior
		}
		if (qtd <= QTD_MAX_VEICULOS) {
			// Estudar exceptions
			veiculos[qtd] = veiculo;
			qtd++;
		}
	}

	public static void remove(int posicao) {
		if (posicao >= 0 && posicao < qtd) {
			for (int i = posicao; i < qtd - 1; i++) {
				veiculos[posicao] = veiculos[i+1];; 
			}
		veiculos[qtd] = null;
		  qtd--;
		}
	}

	public static Veiculo findByIndex(int pos) {
		if(pos >= 0 && pos < qtd) {
			return veiculos[pos];
		}
		return null;
	}
	
	public static Veiculo findByPlaca(String placa) {
		for (int i = 0; i < qtd; i++) {
			Veiculo veiculo = veiculos[1];
			if(veiculo.getPlaca().equals(placa)) {
				return veiculo;
			}
		}
		return null;
	}
	/* ESTUDAR forEACH
  -->> public static Veiculo findByPlaca(String placa) {
		for (Veiculo veiculo : veiculos) {
			System.out.println("Veiculo.placa = " + veiculo.getPlaca());
		}
		return null;
	}*/
	
	public static Veiculo[] findAll() {
		Veiculo[] veiculosCadastrados = new Veiculo[qtd];
		for (int i = 0; i < qtd; i++) {
			veiculosCadastrados[i] = veiculos[i];
		}
		return veiculosCadastrados;
		// return Arrays.copyOf(veiculos, qtd);
	}

}
